const assert = require('assert');
const { Pact } = require('@pact-foundation/pact');

const { getData, postData } = require('../../src/assets/mockData');
const { getTodos, postTodos } = require('../../src/api');

const apiUrl = 'http://localhost:8081';

describe('Consumer Tests todo app', () => {
	const provider = new Pact({
		port: 8081,
		consumer: 'todo-app-client',
		provider: 'todo-app-api',
		pactfileWriteMode: 'update',
	});

	before(() => provider.setup());

	describe('When a requst is sent to Todo API', () => {
		describe('When a GET requst is sent to Todo API', () => {
			before(() => {
				return provider.addInteraction({
					uponReceiving: 'All todos',
					withRequest: {
						path: '/todos',
						method: 'GET',
					},
					willRespondWith: {
						status: 200,
						body: getData,
					},
				});
			});
			it('Will recive the list of todo', async () => {
				getTodos(apiUrl)
					.then((r) => {
						assert.ok(r.status === 200);
					})
					.catch((error) => {
						throw new Error(error.message || error);
					});
			});
		});
		describe('When a POST requst is sent to Todo API', () => {
			before(() => {
				return provider.addInteraction({
					uponReceiving: 'Post a new todo',
					withRequest: {
						path: '/todos',
						method: 'POST',
						body: postData,
					},
					willRespondWith: {
						status: 201,
					},
				});
			});

			it('Will recive the status code 201', async () => {
				postTodos(apiUrl, postData)
					.then((r) => {
						assert.ok(r.status === 201);
					})
					.catch((error) => {
						throw new Error(error.message || error);
					});
			});
		});
	});
	after(() => provider.finalize());
});

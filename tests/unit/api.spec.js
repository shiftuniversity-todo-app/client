import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const { getData, postData } = require('../../src/assets/mockData');
const { getTodos, postTodos } = require('../../src/api');

const mock = new MockAdapter(axios);

const apiUrl = 'http://localhost:8081';

describe('Api functions tests', () => {
	afterAll(() => mock.restore());
	beforeEach(() => mock.reset());

	it('GET todos', async () => {
		mock.onGet(apiUrl + '/todos').reply(200, getData);

		const response = await getTodos(apiUrl);

		expect(response.status).toBe(200);
	});

	it('POST todos', async () => {
		mock.onPost(apiUrl + '/todos').reply(201);

		const response = await postTodos(apiUrl, postData);

		expect(response.status).toBe(201);
	});
});

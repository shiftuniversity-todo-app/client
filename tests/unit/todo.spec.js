import { shallowMount } from '@vue/test-utils';
import Todo from '@/components/Todo.vue';
import { getData } from '../../src/assets/mockData';

let wrapper;
let title;

describe('Todo.vue', () => {
	title = 'Todo App';
	wrapper = shallowMount(Todo, {
		propsData: { title },
		data() {
			return { todos: getData.todos };
		},
	});
	it('renders props.title when passed', () => {
		expect(wrapper.text()).toMatch(title);
	});
	it('Textbox renders properly', () => {
		expect(wrapper.find('#todo-textbox').exists()).toBeTruthy();
	});
	it('Add button renders properly', () => {
		expect(wrapper.find('#todo-add').exists()).toBeTruthy();
	});
	it('List rennders properly', () => {
		expect(wrapper.find('#todo-list').exists()).toBeTruthy();
	});

	it('List items rendered properly', () => {
		getData.todos.map((data, i) => {
			expect(wrapper.find(`#list-item-${i}`).text()).toMatch(data.todo);
		});
	});
});

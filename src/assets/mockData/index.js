const postData = {
	todo: 'bla ',
};

const getData = {
	todos: [
		{
			_id: '1',
			todo: 'bla',
			_v: 0,
		},
		{
			_id: '2',
			todo: 'bla bla',
			_v: 0,
		},
		{
			_id: '3',
			todo: 'bla bla bla',
			_v: 0,
		},
	],
};

module.exports = {
	postData,
	getData,
};

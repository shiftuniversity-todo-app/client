const axios = require('axios');

const getTodos = async (apiUrl) => {
	try {
		return await axios.get(apiUrl + '/todos');
	} catch (error) {
		throw new Error(error.message || error);
	}
};

const postTodos = async (apiUrl, postData) => {
	try {
		return await axios.post(apiUrl + '/todos', postData);
	} catch (error) {
		throw new Error(error.message || error);
	}
};

module.exports = {
	getTodos,
	postTodos,
};
